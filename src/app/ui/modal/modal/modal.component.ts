import { Component, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  @Input() title = 'Cocktail details';
  @Input() isOpen = false;
  @Input() close = new EventEmitter<void>();
  constructor() { }

  onClose(){
    this.close.emit();
  }
}
