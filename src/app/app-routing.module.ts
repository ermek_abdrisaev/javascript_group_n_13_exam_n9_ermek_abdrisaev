import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';
import { AllCocktailsComponent } from './all-cocktails/all-cocktails.component';

const routes: Routes = [
  {path: '', component: AllCocktailsComponent},
  {path: 'new-cocktail', component: NewCocktailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
