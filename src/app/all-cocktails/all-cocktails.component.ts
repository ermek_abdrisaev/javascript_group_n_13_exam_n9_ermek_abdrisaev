import { Component, OnDestroy, OnInit } from '@angular/core';
import { Cocktail, Ingredients } from '../shared/cocktails.model';
import { Subscription } from 'rxjs';
import { CocktailService } from '../shared/coctail.service';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'app-all-cocktails',
  templateUrl: './all-cocktails.component.html',
  styleUrls: ['./all-cocktails.component.css']
})
export class AllCocktailsComponent implements OnInit, OnDestroy {
  cocktails!: Cocktail[];
  cocktail: Cocktail | null = null;
  cocktailsChangeSubscription!: Subscription;
  cocktailsFetchingSubscription!: Subscription;
  loading: boolean = false;
  modalOpen = false;

  constructor(
    private cocktailsService: CocktailService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.cocktailsChangeSubscription = this.cocktailsService.cocktailsChange.subscribe((cocktails: Cocktail[]) =>{
      this.cocktails = cocktails.reverse();
    });
    this.cocktailsFetchingSubscription = this.cocktailsService.cocktailsFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.cocktailsService.fetchCocktails();
  }

  openCheckoutModal(id: string){
    this.modalOpen = true;


      this.cocktails.forEach(item => {
        if(id === item.id){
          this.cocktail = item;
        }
      });


    this.cocktailsService.fetchCocktail(id);
  }

  closeCheckoutModal(){
    this.modalOpen = false;
  }

  ngOnDestroy(): void {
    this.cocktailsChangeSubscription.unsubscribe();
    this.cocktailsFetchingSubscription.unsubscribe();
  }



}
