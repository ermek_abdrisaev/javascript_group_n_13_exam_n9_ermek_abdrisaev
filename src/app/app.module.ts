import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CocktailService } from './shared/coctail.service';
import { AllCocktailsComponent } from './all-cocktails/all-cocktails.component';
import { ModalComponent } from './ui/modal/modal/modal.component';
import { ValidateUrlDirective } from './validate-url.directive';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewCocktailComponent,
    AllCocktailsComponent,
    ModalComponent,
    ValidateUrlDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
