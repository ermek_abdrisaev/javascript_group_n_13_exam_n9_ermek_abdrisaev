export class Cocktail {
  constructor(
    public id: string,
    public cocktailName: string,
    public image: string,
    public type: string,
    public description: string,
    public ingredients: Ingredients[],
    public instructions: string
  ){}
}

export class Ingredients {
  constructor(
    public ingName: string,
    public quantity: number,
    public measure: string,
  ){}
}
