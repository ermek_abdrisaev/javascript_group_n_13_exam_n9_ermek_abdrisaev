import { Subject, Subscription } from 'rxjs';
import { Cocktail } from './cocktails.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable()

export class CocktailService {
  cocktailsChange = new Subject<Cocktail[]>();
  cocktailsFetching = new Subject<boolean>();
  cocktailsUploading = new Subject<boolean>();
  cocktailsRemoving = new Subject<boolean>();

  constructor(private http: HttpClient){}

  private cocktails: Cocktail[] = [];

  fetchCocktails(){
    this.cocktailsFetching.next(true);
    this.http.get<{[id: string]: Cocktail}>('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/cocktails.json')
      .pipe(map(result =>{
        return Object.keys(result).map(id =>{
          const cocktailsData = result[id];
          return new Cocktail(
            id,
            cocktailsData.cocktailName,
            cocktailsData.image,
            cocktailsData.type,
            cocktailsData.description,
            cocktailsData.ingredients,
            cocktailsData.instructions,
          );
        });
      }))
      .subscribe(cocktails =>{
        this.cocktails = cocktails;
        this.cocktailsChange.next(this.cocktails.slice());
        this.cocktailsFetching.next(false);
      }, () =>{
        this.cocktailsFetching.next(false);
      });
  }

  fetchCocktail(id: string){
    return this.http.get<Cocktail | null>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/cocktails/${id}.json`)
      .pipe(map(result =>{
        if(!result){
          return null;
        }
        return new Cocktail(id, result.cocktailName, result.image, result.type, result.description, result.ingredients, result.instructions);
      })
      );
  }

  addCocktail(cocktail: Cocktail){
    const body = {
      cocktailName: cocktail.cocktailName,
      image: cocktail.image,
      type: cocktail.type,
      description: cocktail.description,
      ingredients: cocktail.ingredients,
      instructions: cocktail.instructions,
    };
    this.cocktailsUploading.next(true);

    return this.http.post('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/cocktails.json', body)
      .pipe(tap(() =>{
        this.cocktailsUploading.next(false);
      }, () =>{
        this.cocktailsUploading.next(false);
      })
      );
  }
}
