import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';

export const urlValidator = (control: AbstractControl): ValidationErrors | null => {
  const hasHttp = /(http|https):\/\//.test(control.value);
  if (hasHttp) {
    return null;
  }
  return {image: true};
};


@Directive({
  selector: '[appLink]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateUrlDirective,
    multi: true,
  }]
})

export class ValidateUrlDirective implements Validator {

  validate(control: AbstractControl): ValidationErrors | null {
    return urlValidator(control);
  }

}
