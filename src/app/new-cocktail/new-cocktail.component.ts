import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CocktailService } from '../shared/coctail.service';
import { Router } from '@angular/router';
import { Cocktail } from '../shared/cocktails.model';
import { urlValidator } from '../validate-url.directive';

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.css']
})

export class NewCocktailComponent implements OnInit {
  cocktailsForm!: FormGroup;
  isUploading = false;
  cocktailUploadingSubscription!: Subscription;
  isTouched = true;

  constructor(
    private cocktailService: CocktailService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.cocktailUploadingSubscription = this.cocktailService.cocktailsUploading.subscribe((isUploading: boolean) =>{
      this.isUploading = isUploading;
    });

    this.cocktailsForm = new FormGroup({
      cocktailName: new FormControl('', Validators.required),
      image: new FormControl('', [Validators.required, urlValidator]),
      type: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
      instructions: new FormControl('', Validators.required)
    });
  }

  onCreate(){
    const id = Math.random().toString();
    const cocktail = new Cocktail(
      id,
      this.cocktailsForm.value.cocktailName,
      this.cocktailsForm.value.image,
      this.cocktailsForm.value.type,
      this.cocktailsForm.value.description,
      this.cocktailsForm.value.ingredients,
      this.cocktailsForm.value.instructions,
    );
    this.cocktailService.addCocktail(cocktail).subscribe();
    void this.router.navigate(['/']);
  }

  addIngs(){
    this.isTouched = false;
    const ingredients = <FormArray>this.cocktailsForm.get('ingredients');
    const ingGroup = new FormGroup({
      ingName: new FormControl('', Validators.required),
      quantity: new FormControl(null, Validators.required),
      measure: new FormControl('', Validators.required),
    });
    ingredients.push(ingGroup);
  }

  getIngredientsControls(){
    const ingredients = <FormArray>(this.cocktailsForm.get('ingredients'));
    return ingredients.controls;
  }

  removeIng(i: number) {
    const formArr = <FormArray>this.cocktailsForm.get('ingredients');
    formArr.removeAt(i);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailsForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

}
